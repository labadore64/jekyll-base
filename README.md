This is a reduced copy of the basic Jekyll sample pages from [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/). You can view it [here](https://labadore64.gitlab.io/jekyll-base/).

The hope of this project is to provide a few pre-assembled templates for people to use to set up their own statically generated websites through Gitlab Pages without the need to learn Git.

If you want more details about the base template this project was forked from, visit [https://gitlab.com/pages/jekyll](https://gitlab.com/pages/jekyll).

Other templates:
- [Console](https://gitlab.com/labadore64/simple-blog)
